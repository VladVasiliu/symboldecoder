cd libs/zlib/zlib-1.2.11/ && ./configure &&make clean && make && make check && make install && cd ../../..
cd libs/elfutils-0.185/  && autoreconf -i -f && ./configure --enable-maintainer-mode --disable-debuginfod && make clean && make && make check && make install && cd ../..
cd libs/libdwarf-20210528/ && ./configure --enable-dwarfexample --enable-dwarfgen && make clean && make && make check && make install && cd ../..
cd SymbolDecoder/Debug && make clean && make && cd ../..
cd SymbolDecoderTest/Debug && make clean && make && cd .. && ./Debug/SymbolDecoderTest && cd ..
