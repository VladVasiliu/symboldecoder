# Symbol Decoder

The purpose of this tool is to find the address,type and size of a global 
variable / function.

## Usage:
 
 ./SymbolDecoder <Elf file> <expresion>
 
 <Elf file>  -  elf file with debug symbols in DWARF format
 <expresion> -  C like style expresion of a global variable
 
## Example:

```
./SymbolDecoder ARM_TestProject.elf Structure.SubStructure.ui16Int

```
Output:

```
INFO : Elf File : elfs/ARM_TestProject.elf
INFO : Symbol   : Structure.SubStructure.ui16Int
INFO : Found   : Structure.SubStructure.ui16Int
INFO : Address : 00019C9A
INFO : Type    : DW_ATE_unsigned
INFO : Size    : 2
```



Known limitations:
1. In the current implementations maximum of arrays of arrays of arrays can be decoded
(example float32 fl_Cube[4][4][4];)

2. Due to the nature of the program multiple anonymous structure/unions types are not suported.
Example:

```
volatile struct
{
	struct
	{
		int8_t ui8Element5;
	} namedstruct;
	struct
	{
		uint16_t ui16Element1; // ui16Element1 is decodable with the TEST_stStr..ui16Element1
		uint8_t  ui8Element2;
	} ; //  unnamed struct 

	struct
	{ 
	 float fl_Element4; //fl_Element4 cannot be decoded .
	}; // unnamed struct 

} TEST_stStr;
```

## Dependencies : 
libelf,libdwarf,zlib

See the libs folder. These snapshoots were tested with the symbol decoder .

building zlib 
	`./configure && make && make check`
building libelf
	`autoreconf -i -f && ./configure --enable-maintainer-mode --disable-debuginfod && make && make check`
building libdwarf
	'./configure --enable-dwarfexample --enable-dwarfgen && make && make check'

## Building information : 
     
```
git clone https://gitlab.com/VladVasiliu/symboldecoder.git 
cd symboldecoder/SymbolDecoder/Debug/ 
make clean 
make 
```

## Installing 
You can simply use the "SymbolDecoder" program or you can manualy copy it into your local /bin 

 
## Disclaimer : 

The software could include technical or other mistakes, inaccuracies or errors. The software may be out of date, and I make no commitment to update it. Use it on your own risk.!
