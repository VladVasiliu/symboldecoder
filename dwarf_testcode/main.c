
#include <stdio.h>
 

char                    test_char;
unsigned char           test_unsignedchar;
short int               test_shortint;
unsigned short int      test_unsignedshortint;
long int                test_longint;
unsigned long int       test_unsignedlongint;
long long int           test_longlongint;
unsigned long long int  test_unsignedlonglongint;
int                     test_int;
unsigned                test_unsinged;
float                   test_float;
double                  test_double;

char                    test_chararray[10];
unsigned char           test_unsignedchararray[11];
short int               test_shortintarray[12];
unsigned short int      test_unsignedshortintarray[13];
long int                test_longintarray[14];
unsigned long int       test_unsignedlongintarray[15];
long long int           test_longlongintarray[16];
unsigned long long int  test_unsignedlonglongintarray[17];
int                     test_intarray[18];
unsigned                test_unsingedarray[19];
float                   test_floatarray[20];
double                  test_doublearray[21];


char                    test_chararrayarray[21][10];
unsigned char           test_unsignedchararrayarray[20][11];
short int               test_shortintarrayarray[19][12];
unsigned short int      test_unsignedshortintarrayarray[18][13];
long int                test_longintarrayarray[17][14];
unsigned long int       test_unsignedlongintarrayarray[16][15];
long long int           test_longlongintarrayarray[15][16];
unsigned long long int  test_unsignedlonglongintarrayarray[14][17];
int                     test_intarrayarray[13][18];
unsigned                test_unsingedarrayarray[12][19];
float                   test_floatarrayarray[11][20];
double                  test_doublearrayarray[10][21];

struct struct_t
{
char                    test_char;
unsigned char           test_unsignedchar;
short int               test_shortint;
unsigned short int      test_unsignedshortint;
long int                test_longint;
unsigned long int       test_unsignedlongint;
long long int           test_longlongint;
unsigned long long int  test_unsignedlonglongint;
int                     test_int;
unsigned                test_unsinged;
float                   test_float;
double                  test_double;

char                    test_chararray[10];
unsigned char           test_unsignedchararray[11];
short int               test_shortintarray[12];
unsigned short int      test_unsignedshortintarray[13];
long int                test_longintarray[14];
unsigned long int       test_unsignedlongintarray[15];
long long int           test_longlongintarray[16];
unsigned long long int  test_unsignedlonglongintarray[17];
int                     test_intarray[18];
unsigned                test_unsingedarray[19];
float                   test_floatarray[20];
double                  test_doublearray[21];


char                    test_chararrayarray[21][10];
unsigned char           test_unsignedchararrayarray[20][11];
short int               test_shortintarrayarray[19][12];
unsigned short int      test_unsignedshortintarrayarray[18][13];
long int                test_longintarrayarray[17][14];
unsigned long int       test_unsignedlongintarrayarray[16][15];
long long int           test_longlongintarrayarray[15][16];
unsigned long long int  test_unsignedlonglongintarrayarray[14][17];
int                     test_intarrayarray[13][18];
unsigned                test_unsingedarrayarray[12][19];
float                   test_floatarrayarray[11][20];
double                  test_doublearrayarray[10][21];

} test_struct;

union union_t
{
char                    test_char;
unsigned char           test_unsignedchar;
short int               test_shortint;
unsigned short int      test_unsignedshortint;
long int                test_longint;
unsigned long int       test_unsignedlongint;
long long int           test_longlongint;
unsigned long long int  test_unsignedlonglongint;
int                     test_int;
unsigned                test_unsinged;
float                   test_float;
double                  test_double;

char                    test_chararray[10];
unsigned char           test_unsignedchararray[11];
short int               test_shortintarray[12];
unsigned short int      test_unsignedshortintarray[13];
long int                test_longintarray[14];
unsigned long int       test_unsignedlongintarray[15];
long long int           test_longlongintarray[16];
unsigned long long int  test_unsignedlonglongintarray[17];
int                     test_intarray[18];
unsigned                test_unsingedarray[19];
float                   test_floatarray[20];
double                  test_doublearray[21];


char                    test_chararrayarray[21][10];
unsigned char           test_unsignedchararrayarray[20][11];
short int               test_shortintarrayarray[19][12];
unsigned short int      test_unsignedshortintarrayarray[18][13];
long int                test_longintarrayarray[17][14];
unsigned long int       test_unsignedlongintarrayarray[16][15];
long long int           test_longlongintarrayarray[15][16];
unsigned long long int  test_unsignedlonglongintarrayarray[14][17];
int                     test_intarrayarray[13][18];
unsigned                test_unsingedarrayarray[12][19];
float                   test_floatarrayarray[11][20];
double                  test_doublearrayarray[10][21];

} test_union;



struct struct_t test_structarray[2];
union  union_t  test_unionarray[3];

/* ---- Entrypoint -----*/
int main()
{
	return 0;
}
