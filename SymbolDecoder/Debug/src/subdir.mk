################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/SymbolDecoder.c \
../src/die_func.c \
../src/die_ifx.c 

OBJS += \
./src/SymbolDecoder.o \
./src/die_func.o \
./src/die_ifx.o 

C_DEPS += \
./src/SymbolDecoder.d \
./src/die_func.d \
./src/die_ifx.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c src/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -I../../libs/elfutils-0.185/libelf -I../../libs/libdwarf-20210528/libdwarf -I../../libs/zlib/zlib-1.2.11 -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


