/*Copyright (C) 2019  Vlad Vasiliu

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef DIE_IFX_H_
#define DIE_IFX_H_

#define DIE_VAR_NAME_SIZE (1024)

/***************************************************
 * Function    : CU_PrintName
 * Description : Prints the name of the CU
 ***************************************************/
void CU_PrintName(Dwarf_Debug dbg, Dwarf_Die Cu_die);

/***************************************************
 * Function    : DIE_Debug_Type
 * Description : Prints the die debug type
 ***************************************************/
void DIE_Debug_Type(unsigned int tag);
/***************************************************
 * Function    : CU_PrintName
 * Description : Prints the name of the CU
 ***************************************************/
void CU_PrintName(Dwarf_Debug dbg, Dwarf_Die Cu_die);
/***********************************************************************
 * Function    : DIE_PrintTAG
 * Description : Prints the tag name
 ***********************************************************************/
void DIE_PrintTAG(Dwarf_Debug dbg,Dwarf_Die die);
/**********************************************************
 * Function    : DIE_IsVariable
 * Description : return if the current die is a
 * global variable.
 **********************************************************/
int DIE_IsVariable(Dwarf_Debug dbg, Dwarf_Die die);
/**********************************************************
 * Function    : DIE_IsFunction
 * Description : return if the current die is a
 * function
 **********************************************************/
int DIE_IsFunction(Dwarf_Debug dbg, Dwarf_Die die);
/**********************************************************
 * Function    : DIE_IsType
 * Description : return if the current die is a
 * data type
 **********************************************************/
int DIE_IsType(Dwarf_Debug dbg, Dwarf_Die die);

/**********************************************************************
 * Function   : DIE_Get_DIE_Type
 * Description: Get the DIE of the type of the variable using the offset
 * Return : DW_DLV_OK - Success
 **********************************************************************/
int  DIE_Get_DIE_Type(Dwarf_Debug dbg, Dwarf_Die die, Dwarf_Die *child);
/**********************************************************************
 * Function   : DIE_Get_DIE_AT_specification
 * Description: Get the DIE of the type of the variable using the offset
 * Return : DW_DLV_OK - Success
 **********************************************************************/
int  DIE_Get_DIE_AT_specification(Dwarf_Debug dbg, Dwarf_Die die, Dwarf_Die *child);

/***********************************************************************
 * Function    : DIE_Get_ElementArraySize
 * Description : Returns the Element array size
 * Todo        : Rework the function
 ***********************************************************************/
Dwarf_Unsigned DIE_Get_TotalArraySize(Dwarf_Debug dbg, Dwarf_Die die);
/***********************************************************************
 * Function    : DIE_Get_ElementArraySize
 * Description : Returns the Element array size
 * Todo        : Rework the function
 ***********************************************************************/
Dwarf_Unsigned DIE_Get_ElementArraySize(Dwarf_Debug dbg, Dwarf_Die die);
/**********************************************************************************************
 * Function    : DIE_GetArraySize
 * Description : The function return how many elements are in the Array
 **********************************************************************************************/
int DIE_GetArraySize(Dwarf_Debug dbg, Dwarf_Die die);
/**********************************************************************************************
 * Function    : DIE_GetArraySize
 * Description : The function return how many elements are in the Array
 **********************************************************************************************/
int DIE_GetArrayTableSize(Dwarf_Debug dbg, Dwarf_Die die, unsigned dim[3]);
/*******************************************************************************************
 * Function    : DIE_GetDIEBaseTypeName
 * Description : REturn the name of the basetype
 *******************************************************************************************/
int DIE_GetDIEBaseTypeName(Dwarf_Debug dbg, Dwarf_Die die, char *varname );
/************************************************************************************
 * Function    : DIE_GetStaticAddress
 * Description : This function returns the static address of an object
 ************************************************************************************/
int DIE_GetStaticAddress(Dwarf_Debug dbg, Dwarf_Die die , Dwarf_Addr *Addr);
/************************************************************************************
 * Function    : DIE_GetFnStaticAddress
 * Description : This function returns the static address of an function
 ************************************************************************************/
void DIE_GetFnStaticAddress(Dwarf_Debug dbg, Dwarf_Die die , Dwarf_Addr *Addr_hi,Dwarf_Addr *Addr_low);
/*******************************************************************************************
 * Function    : DIE_GetDIEBaseTypeName
 * Description : Return of the size in bytes of the DIE
 *******************************************************************************************/
int DIE_GetDIESize(Dwarf_Debug dbg, Dwarf_Die die, Dwarf_Unsigned *size);
/*****************************************************************************
 * Function    : DIEGetDieName
 * Description : copies the name of the die in the die_name string
 ***************************************************************************/
int DIE_GetDIE_Name(Dwarf_Debug dbg, Dwarf_Die die,char *die_name);
/*****************************************************************************
 * Function    : DIE_GetDIELocation
 * Description : returns the attribute location as an offset
 ***************************************************************************/
void DIE_GetDIELocation(Dwarf_Debug dbg, Dwarf_Die die,Dwarf_Signed *off);

#endif /* DIE_IFX_H_ */
