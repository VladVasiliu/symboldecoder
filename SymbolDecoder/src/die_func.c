/*Copyright (C) 2019  Vlad Vasiliu

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include "dwarf.h"
#include "libdwarf.h"
#include "die_func.h"
#include "die_ifx.h"
#include "Log.h"
int DIE_CompareSubstrings(char *s1,char *s2)
{
	int ret =0;
	/* ---- Check input parameters ---- */
	if((s1 !=NULL) &&(s2 != NULL))
	{
		size_t size_s1 = strlen(s1);
		size_t size_s2 = strlen(s2);

		size_s1 = (size_s1 > DIE_VAR_NAME_SIZE)? DIE_VAR_NAME_SIZE: size_s1;
		size_s2 = (size_s2 > DIE_VAR_NAME_SIZE)? DIE_VAR_NAME_SIZE: size_s2;


		ret = strncmp(s1,s2,size_s1);

		if(size_s2 > size_s1)
		{
			switch(s2[size_s1])
			{
			case '.':
			case '[':
			case ']':
			{
				/* ---- This cases are excepted . Nothing to do ----*/
			}break;
			default:
			{
				return -1;
			} break;

			}

		}
	}
	return ret;
}


int DIE_CheckCompositeMembers(Dwarf_Debug dbg, Dwarf_Die die , Dwarf_Die *last_die, unsigned long *offset,char *symbol,char *typename,unsigned *checkedindex)
{
	Dwarf_Die   die_aux     =  die;
	Dwarf_Die   die_sibling =  die;
	Dwarf_Half  tag         =  0;
	Dwarf_Error error       =  0;
	int not_found_break     =  0;
	int not_found           =  1;
	int res                 =  0;
	char tag_name[DIE_VAR_NAME_SIZE];

	do
	{
		/* ---- Check the tag of the die ---- */
		if(dwarf_tag(die_sibling,&tag,&error) == DW_DLV_OK)
		{
			if(DW_TAG_member==tag)
			{
				memset(tag_name,0,DIE_VAR_NAME_SIZE);

				/*----Get the tags name ----*/
				if(DIE_GetDIE_Name(dbg,die_sibling,tag_name) == DW_DLV_OK)
				{
					OutLog("DEBUG : DIE_CheckCompositeMembers : DW_TAG_member :%s\n",tag_name);
				}
				/* ---- Compare structure element with the symbol ----*/
				if(DIE_CompareSubstrings((char *)tag_name,(char *)&symbol[(*checkedindex)]) == 0)
				{
					Dwarf_Signed offset_aux =0;

					DIE_GetDIELocation(dbg,die_sibling,&offset_aux);

					OutLog("DEBUG : DIE_CheckCompositeMembers : DIE_GetDIELocation=%d\n",(int)offset_aux);

					(*offset)       += offset_aux;
					(*checkedindex) += strlen(tag_name);

					if((*checkedindex)> strlen(symbol))
					{
						not_found_break=1;
						OutLog("DEBUG : DIE_CheckCompositeMembers : CHeck index exceeded\n");
					}
					else
					{
						not_found = 0;
					}
					/* ---- Element was evaluated -----*/
					break;
				}
				else
				{
					OutLog("DEBUG : DIE_CheckCompositeMembers : Variable not found (DIE_VarCmp failed)\n");
				}
			}
			else
			{
				OutLog("DEBUG : DIE_CheckCompositeMembers : Tag is not DW_TAG_member\n");
			}
		}
		else
		{
			OutLog("ERROR : DIE_CheckCompositeMembers : Tag is not  found (dwarf_tag failed) \n");
			not_found_break=1;
		}

		if(die_aux !=die)
		{
			dwarf_dealloc(dbg,die_aux,DW_DLA_DIE);
		}
		/* ---- Goto the next sibling in the tree ----*/
		die_aux=die_sibling;

		/* ---- Get next sibling ----*/
		res = dwarf_siblingof(dbg,die_aux,&die_sibling,&error);

	}while((not_found_break == 0)&&(res != DW_DLV_NO_ENTRY)&&(res != DW_DLV_ERROR));

	*last_die = die_sibling;

	return not_found;
}

int DIE_CheckComposite(Dwarf_Debug dbg, Dwarf_Die die , Dwarf_Die *last_die, unsigned long *offset,char *symbol,char *typename,unsigned *checkedindex)
{
	Dwarf_Error error;
	Dwarf_Die   die_child = NULL;
	int         not_found = 0;
	char        tag_name[DIE_VAR_NAME_SIZE];

	/* ---- See if the element is a structure -----*/
	if(symbol[*checkedindex] == '.')
	{
		OutLog("DEBUG : DIE_CheckComposite Composite search:\n");
		(*checkedindex)++;

		/* ----- The die should have a child where all the structure elements are placed ----*/
		if(dwarf_child(die,&die_child,&error) == DW_DLV_OK)
		{
			if(DIE_GetDIE_Name(dbg,die_child,tag_name) == DW_DLV_OK)
			{
				OutLog("DEBUG : DIE_CheckComposite Child Name is:%s\n",tag_name);
			}

			not_found = DIE_CheckCompositeMembers(dbg,die_child,last_die,offset,symbol,typename,checkedindex);

			if(die_child != *last_die)
			{
				/*---- Dealocate the die ----*/
				dwarf_dealloc(dbg,die_child,DW_DLA_DIE);
			}
		}

	}
	else
	{
		/* ---- Element is not a composite -----*/
		not_found = 1;
	}

	return not_found;
}

int DIE_ExtractArraySizeFromStr(const char *symbol, unsigned *pozinbuffer)
{
	unsigned checkedindex   =*pozinbuffer;
	int      current_index  =-1;

	/* ---- Check if the current index is on a array descriptor----- */
	if(symbol[checkedindex] == '[')
	{
		checkedindex++;
		sscanf(&symbol[checkedindex],"%d",&current_index);
	}
	/* ---- Search until the end of the string for the ']' character ----*/
	while((symbol[checkedindex] != ']')&&(symbol[checkedindex] != 0)&&(checkedindex < DIE_VAR_NAME_SIZE))
	{
		checkedindex++;
	}

	*pozinbuffer = (checkedindex+1);

	OutLog("DEBUG : DIE_ExtractArraySizeFromStr = %d\n",current_index);

	return current_index;
}

Dwarf_Unsigned DIE_CheckType(Dwarf_Debug dbg,Dwarf_Die die, unsigned long *offset, const char *symbol,const char *typename,unsigned *checkedindex_ptr)
{
	Dwarf_Die      die_child    = NULL;
	Dwarf_Die      die_new      = NULL;
	Dwarf_Die      die_parent   = die;
	Dwarf_Half     tag          = 0;
	Dwarf_Unsigned size         = 0;
	Dwarf_Error    error        = 0;
	unsigned       checkedindex = 0;
	int            not_found    = 0;
	int            stop         = 0;

	/* ---- Get the die which contains the type information ----*/
	while((not_found == 0)&&(stop == 0))
	{
		if((DIE_Get_DIE_Type(dbg,die_parent,&die_child)== DW_DLV_OK))
		{
			OutLog("DEBUG : DIE_CheckType : last size=%d | checked_index=%d\n",(int)size,(int)checkedindex);

			if(dwarf_tag(die_child,&tag,&error) == DW_DLV_OK)
			{
				DIE_Debug_Type(tag);

				switch(tag)
				{
				case DW_TAG_base_type:
				{
					size = 0;
					DIE_GetDIESize(dbg,die_child,&size);
					DIE_GetDIEBaseTypeName(dbg,die_child,(char*)typename);
					stop=1;
				}break;
				case DW_TAG_enumeration_type:
				{
					size = 0;
					DIE_GetDIESize(dbg,die_child,&size);
					strcpy((char*)typename,"_ENUM_");
					stop = 1;
				}break;
				case DW_TAG_subroutine_type:
				{
					size = 0;
					DIE_GetDIESize(dbg,die_child,&size);
					strcpy((char*)typename,"_CALLBACK_");
					stop = 1;
				}break;
				case DW_TAG_pointer_type:
				{
					size = 0;
					DIE_GetDIESize(dbg,die_child,&size);
					strcpy((char*)typename,"_PTR_");
					stop = 1;
				}break;
				case DW_TAG_array_type:
				{
					int            element_size = 0;
					unsigned       dim[3]={0,0,0};
					int            inputpoz[3]={0,0,0};
					int            ArrayTableSize =0;
					element_size = DIE_Get_ElementArraySize(dbg,die_child);
					OutLog("DEBUG : DIE_CheckType : DW_TAG_array_type : element_size=%d\n",(int)element_size);

					ArrayTableSize = DIE_GetArrayTableSize(dbg,die_child ,dim);
					OutLog("DEBUG : DIE CheckType : DW_TAG_array_type : DIE_GetArrayTableSize = [%d,%d,%d]\n",dim[0],dim[1],dim[2]);


					/* ---- Read the first index ----*/
					if((dim[0] > 0) && (ArrayTableSize > 0))
					{
						inputpoz[0] = DIE_ExtractArraySizeFromStr(symbol,&checkedindex);
						/*  ----- Perform range check for the index read -----*/
						if(inputpoz[0] < 0)
						{
							OutLog("ERROR : DIE CheckType : DW_TAG_array_type : 1-st index not found! assuming 0\n");
							inputpoz[0] = 0;

						}

						if(inputpoz[0] >= dim[0])
						{
							OutLog("ERROR : DIE CheckType : DW_TAG_array_type : 1-st index out of range !(max %d | val %d) assuming the max val \n",dim[0],inputpoz[0]);
							inputpoz[0] = dim[0]-1;

						}
					}
					/* ---- Read the second index  index if necessary ----*/
					if((dim[1] > 0)&& (ArrayTableSize > 1))
					{
						inputpoz[1] = DIE_ExtractArraySizeFromStr(symbol,&checkedindex);
						/*  ----- Perform range check for the index read -----*/
						if(inputpoz[1] < 0)
						{
							OutLog("ERROR : DIE CheckType : DW_TAG_array_type : 2-nd index not found! assuming 0\n");
							inputpoz[2] = 0;

						}
						if(inputpoz[1] >= dim[1])
						{
							OutLog("ERROR : DIE CheckType : DW_TAG_array_type : 2-nd index out of range !(max %d | val %d) assuming the max val\n",dim[1],inputpoz[1]);
							inputpoz[1] = dim[1]-1;

						}
					}
					/* ---- Read the 3-rd index  index if necessary ----*/
					if((dim[2] > 0)&& (ArrayTableSize > 2))
					{
						inputpoz[2] = DIE_ExtractArraySizeFromStr(symbol,&checkedindex);
						/*  ----- Perform range check for the index read -----*/
						if(inputpoz[2] < 0)
						{
							OutLog("ERROR : DIE CheckType : DW_TAG_array_type : 3-rd index not found! assuming 0\n");
							inputpoz[2] = 0;
						}
						if(inputpoz[2] >= dim[2])
						{
							OutLog("ERROR : DIE CheckType : DW_TAG_array_type : 3-rd index out of range !(max %d | val %d) assuming the max val\n",dim[2],inputpoz[2]);
							inputpoz[2] = dim[2]-1;
						}
					}



					/*ToDo : handle only max array of [][][] possible  */

					if(not_found ==0)
					{

						switch(ArrayTableSize)
						{
						case 1:
						{
							/* ---- Calculate the expected offset from an array ----*/
							*offset+= inputpoz[0]*element_size;
						}break;
						case 2:
						{
							/* ---- Calculate the expected offset from an array ----*/
							*offset+= inputpoz[1]*element_size;
							*offset+= inputpoz[0]*element_size*dim[1];
						}break;
						case 3:
						{
							*offset+= inputpoz[2]*element_size;
							*offset+= inputpoz[1]*element_size*dim[1];
							*offset+= inputpoz[0]*element_size*dim[1]*dim[2];
						}break;
						default:{};break;
						}



					}

				}break;
				case DW_TAG_structure_type:
				case DW_TAG_class_type:
				{
					size = 0;
					DIE_GetDIESize(dbg,die_child,&size);
					strcpy((char*)typename,"_STRUCT_");
					/* ---- Perform a search inside the structure ----*/
					die_new = NULL;
					not_found = DIE_CheckComposite(dbg,die_child,&die_new,offset,(char*)symbol,(char*)typename,&checkedindex);

					/* ---- Check if the die must be changed after the composite analisys ----*/
					if(die_new !=NULL)
					{
						dwarf_dealloc(dbg,die_child,DW_DLA_DIE);
						die_child = die_new;
					}

				}break;
				case DW_TAG_union_type:
				{
					unsigned long dummy_offset = 0u;
					size = 0;
					DIE_GetDIESize(dbg,die_child,&size);
					strcpy((char*)typename,"_UNION_");
					/* ---- Perform a search inside the structure ----*/
					die_new = NULL;
					not_found = DIE_CheckComposite(dbg,die_child,&die_new,&dummy_offset,(char*)symbol,(char*)typename,&checkedindex);

					/* ---- Check if the die must be changed after the composite analisys ----*/
					if(die_new !=NULL)
					{
						dwarf_dealloc(dbg,die_child,DW_DLA_DIE);
						die_child = die_new;
					}

				}break;
				}
			}

			/* ---- Dealocate the die ----*/
			if(die_parent != die)
			{
				dwarf_dealloc(dbg,die_parent,DW_DLA_DIE);
			}
			/* ---- Go  to next element ---- */
			die_parent = die_child;
		}
		else
		{
			OutLog("ERROR : DIE_CheckType :DIE_Get_DIE_Type\n");
			break;
		}
	}
	/* ----- return the string index which was checked ----*/
	if(checkedindex_ptr != NULL)
	{
		*checkedindex_ptr = checkedindex;
	}
	return size;
}

int DIE_HandleFunction(Dwarf_Debug dbg, Dwarf_Die die, const char *symbol)
{
	Dwarf_Addr Addr_hi=0;
	Dwarf_Addr Addr_lo=0;
	char       name[DIE_VAR_NAME_SIZE];
	int        found = 0;

	if(DIE_IsFunction(dbg,die)!=0)
	{
		/* ---- Clean working data ---- */
		memset(name,0,DIE_VAR_NAME_SIZE);

		/* ---- Get the name of the function ----*/
		DIE_GetDIE_Name(dbg,die,name);

		/* ---- Check if it is the expected function -----*/
		if(DIE_CompareSubstrings((char*)name,(char*)symbol) == 0)
		{
			/* ---- Get the Address of the Function ----*/
			DIE_GetFnStaticAddress(dbg,die,&Addr_hi,&Addr_lo);

			printf("INFO : Type        : FUNCTION\n");

			/* ---- Check if the function is not optimised -----*/
			if(Addr_lo!=0)
			{
				printf("INFO : Address Low : %08lX\n",(unsigned long)Addr_lo);
				found=1;
			}
			if(Addr_hi!=0)
			{
				printf("INFO : Address High: %08lX\n",(unsigned long)Addr_hi);
				found=1;
			}

		}
	}

	return found;
}

int DIE_HandleVariable(Dwarf_Debug dbg, Dwarf_Die die, const char *symbol)
{
	Dwarf_Addr     Addr         = 0;
	Dwarf_Die      die_parent   = die;
	Dwarf_Die      die_child    = die;
	Dwarf_Unsigned size         = 0;
	unsigned long  offset       = 0;
	unsigned       checkedindex = 0u;
	char name[DIE_VAR_NAME_SIZE];
	char varname[DIE_VAR_NAME_SIZE];
	char type[DIE_VAR_NAME_SIZE];
	int found = 0;

	/* ---- Check if it is a variable ----*/
	if(DIE_IsVariable(dbg,die)!=0)
	{
		/*---- Get the address of the variable ----*/
		DIE_GetStaticAddress(dbg,die,&Addr);

		/* ---- Check if it is optimized ----*/
		if(Addr > 0)
		{
			memset(name,0,DIE_VAR_NAME_SIZE);
			/* ---- Check for a name ----*/
			if(DIE_GetDIE_Name(dbg,die_parent,name)!= DW_DLV_OK)
			{
				/* ---- Name not found . Search for specification ---- */
				if(DIE_Get_DIE_AT_specification(dbg,die_parent,&die_child)!= DW_DLV_OK)
				{
					printf("WARNING : Die with the address %08lX does not have a name/Specification \n",(unsigned long)Addr);
				}
				else
				{
					/* ---- Specification was found . Get the name ----*/
					if(DIE_GetDIE_Name(dbg,die_child,name)== DW_DLV_OK)
					{
						/* ---- Check if  it is the expected variable ---- */
						if(DIE_CompareSubstrings((char*)name,(char*)symbol)==0)
						{
							/* ---- yes it is ----*/
							die_parent = die_child;

							/* ---- Clean working data ----*/
							memset(varname,0,DIE_VAR_NAME_SIZE);
							memset(type,0,DIE_VAR_NAME_SIZE);
							/* ---- Check the type of the variable ----*/
							size = DIE_CheckType(dbg,die_parent,&offset,&symbol[strlen(name)],type,&checkedindex);
							/* ----  Correct the checked index since we already considered the variable name ----*/
							checkedindex += strlen(name);
							/* ---- limit the checkedindex  anyway -----*/
							checkedindex = (checkedindex <DIE_VAR_NAME_SIZE)? (checkedindex): (DIE_VAR_NAME_SIZE);
							/* ---- Copy the decoded name into the varname buffer ----*/
							memcpy(varname,symbol,checkedindex);

							/* ---- Print the results -----*/
							printf("INFO : Found   : %s\n",varname);
							printf("INFO : Address : %08lX\n",(unsigned long)(Addr+offset));
							printf("INFO : Type    : %s\n",type);
							printf("INFO : Size    : %ld\n",(unsigned long)size);

							OutLog("INFO : Base Address : %08lX\n",(unsigned long)Addr);
							OutLog("INFO : Offset       : %04lX\n",(unsigned long)offset);
							found = 1;

							dwarf_dealloc(dbg,die_parent,DW_DLA_DIE);
						}
					}
				}
			}
			else
			{
				if(DIE_CompareSubstrings((char*)name,(char*)symbol)==0)
				{
					/* ---- Clean working data ----*/
					memset(varname,0,DIE_VAR_NAME_SIZE);
					memset(type,0,DIE_VAR_NAME_SIZE);
					/* ---- Check the type of the variable ----*/
					size = DIE_CheckType(dbg,die_parent,&offset,&symbol[strlen(name)],type,&checkedindex);
					/* ----  Correct the checked index since we already considered the variable name ----*/
					checkedindex += strlen(name);
					/* ---- limit the checkedindex  anyway -----*/
					checkedindex = (checkedindex <DIE_VAR_NAME_SIZE)? (checkedindex): (DIE_VAR_NAME_SIZE);
					/* ---- Copy the decoded name into the varname buffer ----*/
					memcpy(varname,symbol,checkedindex);

					/* ---- Print the results -----*/
					printf("INFO : Found   : %s\n",varname);
					printf("INFO : Address : %08lX\n",(unsigned long)(Addr+offset));
					printf("INFO : Type    : %s\n",type);
					printf("INFO : Size    : %ld\n",(unsigned long)size);

					OutLog("INFO : Base Address : %08lX\n",(unsigned long)Addr);
					OutLog("INFO : Offset       : %04lX\n",(unsigned long)offset);
					found = 1;
				}
			}
		}

	}

	return found;
}


int DIE_HandleType(Dwarf_Debug dbg, Dwarf_Die die, const char *symbol)
{
	Dwarf_Die      die_parent   = die;
	Dwarf_Unsigned size         = 0;
	unsigned long  offset       = 0;
	unsigned       checkedindex = 0u;
	char name[DIE_VAR_NAME_SIZE];
	char typename[DIE_VAR_NAME_SIZE];
	char type[DIE_VAR_NAME_SIZE];
	int found = 0;

	/* ---- Check if the die has a type ----*/
	DIE_GetDIE_Name(dbg,die_parent,name);

	if(DIE_IsType(dbg,die)!=0)
	{
		memset(name,0,DIE_VAR_NAME_SIZE);
		DIE_GetDIE_Name(dbg,die_parent,name);
		if(DIE_CompareSubstrings((char*)name,(char*)symbol)==0)
		{
			/* ---- Clean working data ----*/
			memset(typename,0,DIE_VAR_NAME_SIZE);
			memset(type,0,DIE_VAR_NAME_SIZE);
			/* ---- Check the type of the variable ----*/
			size = DIE_CheckType(dbg,die_parent,&offset,&symbol[strlen(name)],type,&checkedindex);
			/* ----  Correct the checked index since we already considered the variable name ----*/
			checkedindex += strlen(name);
			/* ---- limit the checkedindex  anyway -----*/
			checkedindex = (checkedindex <DIE_VAR_NAME_SIZE)? (checkedindex): (DIE_VAR_NAME_SIZE);
			/* ---- Copy the decoded name into the typename buffer ----*/
			memcpy(typename,symbol,checkedindex);

			/* ---- Print the results -----*/
			printf("INFO : Found   : %s\n",typename);
			printf("INFO : Address : %08lX\n",(unsigned long)(offset));
			printf("INFO : Type    : %s\n",type);
			printf("INFO : Size    : %ld\n",(unsigned long)size);
			printf("INFO : Offset       : %04lX\n",(unsigned long)offset);
			found = 1;
		}
	}



	return found;
}
