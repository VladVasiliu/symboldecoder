/*Copyright (C) 2019  Vlad Vasiliu

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/


#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "dwarf.h"
#include "libdwarf.h"
#include "die_ifx.h"
#include "Log.h"

/***************************************************
 * Function    : DIE_Debug_Type
 * Description : Prints the die debug type
 ***************************************************/
void DIE_Debug_Type(unsigned int tag)
{
	const char *s=NULL;
	dwarf_get_TAG_name(tag,&s);
	OutLog("DEBUG : DieDebugType :%s\n",s);
}


/***************************************************
 * Function    : CU_PrintName
 * Description : Prints the name of the CU
 ***************************************************/
void CU_PrintName(Dwarf_Debug dbg, Dwarf_Die Cu_die)
{
	char buffer[DIE_VAR_NAME_SIZE];
	int res=0;

	/* ---- Clear working data ----*/
	memset(buffer,0,DIE_VAR_NAME_SIZE);

	/* ---- return the name of the die ----*/
	res = DIE_GetDIE_Name(dbg,Cu_die,buffer);


	if(res == DW_DLV_OK)
	{
		OutLog("DEBUG : CU NAME :%s\n",buffer);
	}
	else
	{
		OutLog("DEBUG : CU has no name\n");
	}

}
/***********************************************************************
 * Function    : DIE_PrintTAG
 * Description : Prints the tag name
 ***********************************************************************/
void DIE_PrintTAG(Dwarf_Debug dbg,Dwarf_Die die)
{
	Dwarf_Error error = 0;
	int res           = 0;
	Dwarf_Half   tag  = 0;
	char *name        = NULL;

	/* ---- Get the  Die type ---- */
	res = dwarf_tag(die,&tag,&error);

	/* ---- Check for errors  ----*/
	if(res == DW_DLV_OK)
	{
		dwarf_get_TAG_name(tag,(const char **)&name);
		OutLog("DEBUG : TAG Name :%s\n",name);
	}
	else
	{
		OutLog("ERROR : Error in reading the TAG Name\n");
	}
}

/**********************************************************
 * Function    : DIE_IsVariable
 * Description : return if the current die is a
 * global variable.
 **********************************************************/
int DIE_IsVariable(Dwarf_Debug dbg, Dwarf_Die die)
{
	int ret            = 0;
	int res            = 0;
	Dwarf_Error error  = 0;
	Dwarf_Half  tag    = 0;

	/* ----- Get the Type of the DIE ---- */
	res = dwarf_tag(die,&tag,&error);
	if(res == DW_DLV_OK)
	{
		/* ---- Check if the DIE is a variable ---- */
		if(DW_TAG_variable == tag)
		{
			ret = 1;
		}
	}

	return ret;
}

/**********************************************************
 * Function    : DIE_IsFunction
 * Description : return if the current die is a
 * function
 **********************************************************/
int DIE_IsFunction(Dwarf_Debug dbg, Dwarf_Die die)
{
	int ret            = 0;
	int res            = 0;
	Dwarf_Error error  = 0;
	Dwarf_Half  tag    = 0;

	/* ----- Get the Type of the DIE ---- */
	res = dwarf_tag(die,&tag,&error);
	if(res == DW_DLV_OK)
	{
		/* ---- Check if the DIE is a Function ---- */
		if(DW_TAG_subprogram == tag)
		{
			ret = 1;

		}
	}

	return ret;
}

/**********************************************************
 * Function    : DIE_IsType
 * Description : return if the current die is a
 * data type
 **********************************************************/
int DIE_IsType(Dwarf_Debug dbg, Dwarf_Die die)
{
	int ret            = 0;
	int res            = 0;
	Dwarf_Error error  = 0;
	Dwarf_Half  tag    = 0;

	/* ----- Get the Type of the DIE ---- */
	res = dwarf_tag(die,&tag,&error);
	if(res == DW_DLV_OK)
	{
		/* ---- Check if the DIE is a Function ---- */
		if((DW_TAG_base_type == tag)||
				(DW_TAG_enumeration_type == tag)||
				(DW_TAG_subroutine_type == tag)||
				(DW_TAG_pointer_type == tag)||
				(DW_TAG_array_type == tag)||
				(DW_TAG_structure_type == tag)||
				(DW_TAG_class_type == tag)||
				(DW_TAG_union_type== tag)||
				(DW_TAG_typedef == tag))
		{
			ret = 1;

		}
	}

	return ret;
}
/**********************************************************************
 * Function   : DIE_Get_DIE_Type
 * Description: Get the DIE of the type of the variable using the offset
 * Return : DW_DLV_OK - Success
 **********************************************************************/
int  DIE_Get_DIE_Type(Dwarf_Debug dbg, Dwarf_Die die, Dwarf_Die *child)
{
	Dwarf_Attribute  Type; /* DW_AT_Type */
	Dwarf_Error      error = 0;
	Dwarf_Off        Offset;
	int res = DW_DLV_ERROR;

	/* ---- Get the address of the variable ----*/
	res = dwarf_attr(die,DW_AT_type,&Type,&error);

	/* ---- Check if there is any kind of Location attribute ----*/
	if(res == DW_DLV_OK )
	{
		/* ---- Get the Offset of the type ---- */
		res = dwarf_global_formref(Type,&Offset,&error);

		if(res == DW_DLV_OK)
		{
			/* ---- Get the die using the offset ----*/
			res = dwarf_offdie(dbg,Offset,child,&error);
		}
	}
	else
	{
		OutLog("ERROR : Error in function DIE_Get_DIE_Type\n");
	}

	return res;
}


/**********************************************************************
 * Function   : DIE_Get_DIE_AT_specification
 * Description: Get the DIE of the type of the variable using the offset
 * Return : DW_DLV_OK - Success
 **********************************************************************/
int  DIE_Get_DIE_AT_specification(Dwarf_Debug dbg, Dwarf_Die die, Dwarf_Die *child)
{
	Dwarf_Attribute  Type; /* DW_AT_Type */
	Dwarf_Error      error = 0;
	Dwarf_Off        Offset;
	int res = DW_DLV_ERROR;

	/* ---- Get the address of the variable ----*/
	res = dwarf_attr(die,DW_AT_specification,&Type,&error);

	/* ---- Check if there is any kind of Location attribute ----*/
	if(res == DW_DLV_OK )
	{
		/* ---- Get the Offset of the type ---- */
		res = dwarf_global_formref(Type,&Offset,&error);

		if(res == DW_DLV_OK)
		{
			/* ---- Get the die using the offset ----*/
			res = dwarf_offdie(dbg,Offset,child,&error);
		}
	}
	else
	{
		OutLog("ERROR : Error in function DIE_Get_DIE_AT_specification\n");
	}

	return res;
}
/***********************************************************************
 * Function    : DIE_Get_ElementArraySize
 * Description : Returns the Element array size
 * Todo        : Rework the function
 ***********************************************************************/
Dwarf_Unsigned DIE_Get_TotalArraySize(Dwarf_Debug dbg, Dwarf_Die die)
{
	Dwarf_Unsigned  size_loc=0;
	Dwarf_Attribute Attr;
	Dwarf_Error     error=0;
	Dwarf_Die       die_child = die;
	Dwarf_Die       die_aux = NULL;

	int res = 0;
	int done =0;

	do
	{
		DIE_PrintTAG(dbg,die_child);

		/*----  Get member location of the variable  ----*/
		res = dwarf_attr(die_child,DW_AT_byte_size,&Attr,&error);

		if(res == DW_DLV_OK)
		{
			res = dwarf_formudata(Attr,&size_loc,&error);
			if(res == DW_DLV_OK)
			{

				OutLog(" INFO : DIE_Get_TotalArraySize = %d",(int)size_loc);
			}
			else
			{
				OutLog("ERROR : Error in function DIE_Get_TotalArraySize : Formudata not found!\n");
				done = 1u;
			}

			if(die_child != die)
			{
				dwarf_dealloc(dbg,die_aux,DW_DLA_DIE);
			}
		}
		else
		{
			die_aux = die_child;
			DIE_PrintTAG(dbg,die_child);
			/* ---- Goto the next child ----*/
			if(DIE_Get_DIE_Type(dbg,die_aux,&die_child) != DW_DLV_OK)
			{
				//if(dwarf_child(dbg,die_aux,&die_child,&error) != DW_DLV_OK)
				{
					done =1;
				}
			}

			if(die != die_child)
			{
				dwarf_dealloc(dbg,die_child,DW_DLA_DIE);
			}

		}
	}while((res == DW_DLV_OK)&&(done == 0));
	return size_loc;
}
/***********************************************************************
 * Function    : DIE_Get_ElementArraySize
 * Description : Returns the Element array size
 * Todo        : Rework the function
 ***********************************************************************/
Dwarf_Unsigned DIE_Get_ElementArraySize(Dwarf_Debug dbg, Dwarf_Die die)
{
	Dwarf_Unsigned  size_loc=0;
	Dwarf_Attribute Attr;
	Dwarf_Half      Form=0;
	Dwarf_Error     error=0;
	Dwarf_Die       die_child = NULL;
	Dwarf_Die       die_aux = die;

	int res = 0;
	int done =0;

	do
	{
		res = DIE_Get_DIE_Type(dbg,die_aux,&die_child);

		if(res == DW_DLV_OK)
		{
			DIE_PrintTAG(dbg,die_child);

			/*---- Check for the location attribute ----*/
			if(dwarf_attr(die_child,DW_AT_byte_size,&Attr,&error) == DW_DLV_OK)
			{
				dwarf_whatform(Attr,&Form,&error);

				if(dwarf_formudata(Attr,&size_loc,&error)== DW_DLV_OK)
				{
					done = 1u;
					OutLog("DEBUG : DIE_Get_ElementArraySize = %d\n",(int)size_loc);
				}
				else
				{
					OutLog("DEBUG : DIE_Get_ElementArraySize : Formudata for DW_AT_byte_size not found!(%d)\n",Form);
				}

			}
			else
			{
				OutLog("DEBUG : DIE_Get_ElementArraySize : DW_AT_byte_size not present\n");
			}

			if(die_aux != die)
			{
				dwarf_dealloc(dbg,die_aux,DW_DLA_DIE);
			}

			die_aux = die_child;
		}
	}while((res == DW_DLV_OK)&&(done == 0));

	return size_loc;
}
int DIE_GetUpperBound(Dwarf_Debug dbg, Dwarf_Die die)
{
	Dwarf_Attribute Size;
	Dwarf_Error     error = 0;
	Dwarf_Signed    data  = 0;
	Dwarf_Unsigned  data_u= 0;
	int             res = 0;
	/* ---- Get the upper bound of the array ----*/
	res = dwarf_attr(die,DW_AT_upper_bound,&Size,&error);

	if(res == DW_DLV_OK)
	{
		/* ----  Interpret data as signed data ----*/
		res = dwarf_formsdata(Size,&data,&error);

		if(res == DW_DLV_OK)
		{
			/* ---- The Array size is data + 1 since the upper bound is the max index possible ---- */
			data++;
		}
		else
		{
			/* ----  Interpret data as unsigned data ----*/
			res = dwarf_formudata(Size,&data_u,&error);

			if(res == DW_DLV_OK)
			{
				data = (Dwarf_Signed)data_u+1;
			}
			else
			{
				OutLog("ERROR :  Error in function DIE_GetArraySize : dwarf_formudata and dwarf_formsdata failed\n");
				data =0;
			}
		}
	}
	else
	{
		OutLog("DEBUG : DIE_GetUpperBound : No upper bound detected\n");
	}

	return data;
}
/**********************************************************************************************
 * Function    : DIE_GetArraySize
 * Description : The function return how many elements are in the Array
 **********************************************************************************************/
int DIE_GetArrayTableSize(Dwarf_Debug dbg, Dwarf_Die die, unsigned dim[3])
{
	Dwarf_Error     error = 0;
	Dwarf_Die       die1 = NULL;
	Dwarf_Die       die2 = NULL;
	Dwarf_Die       die3 = NULL;
	int             ArrayTableSize=0;
	/* ---- Get the next child of the element --- */
	if((dwarf_child(die,&die1,&error)) == DW_DLV_OK)
	{
		/* ---- Extract the size ----*/
		dim[0]= DIE_GetUpperBound(dbg,die1);

		/* ---get the [][y] size of a table if there is any -----*/
		if((dwarf_siblingof(dbg,die1,&die2,&error)) == DW_DLV_OK)
		{
			/* ---- Extract the size ----*/
			dim[1]= DIE_GetUpperBound(dbg,die2);

			/* ---get the [][][z] size of a table if there is any -----*/
			if((dwarf_siblingof(dbg,die2,&die3,&error)) == DW_DLV_OK)
			{
				/* ---- Extract the size ----*/
				dim[2]= DIE_GetUpperBound(dbg,die3);

				ArrayTableSize = 3;

				OutLog("DEBUG : DIE_GetArrayTableSize : Three dimension detected\n");

				dwarf_dealloc(dbg,die3,DW_DLA_DIE);
			}
			else
			{
				ArrayTableSize = 2;

				OutLog("DEBUG : DIE_GetArrayTableSize : Two dimension detected\n");
			}

			dwarf_dealloc(dbg,die2,DW_DLA_DIE);
		}
		else
		{
			ArrayTableSize = 1;

			OutLog("DEBUG : DIE_GetArrayTableSize : One dimension detected\n");
		}


		dwarf_dealloc(dbg,die1,DW_DLA_DIE);
	}
	else
	{
		OutLog("ERROR :  Error in function DIE_GetArrayTableSize : Was expected that the die has a child (dwarf_child failed)\n");
		dim[0] = 0;
		dim[1] = 0;
		dim[2] = 0;
	}


	return ArrayTableSize;
}

/**********************************************************************************************
 * Function    : DIE_GetArraySize
 * Description : The function return how many elements are in the Array
 **********************************************************************************************/
int DIE_GetArraySize(Dwarf_Debug dbg, Dwarf_Die die)
{
	Dwarf_Attribute Size;
	Dwarf_Error     error = 0;
	Dwarf_Signed    data  =-1;
	Dwarf_Unsigned  data_u= 0;
	Dwarf_Die       next_die;
	int res = 0;

	/* ---- Get the next child of the element --- */
	res = dwarf_child(die,&next_die,&error);

	if(res == DW_DLV_OK)
	{
		/* ---- Get the upper bound of the array ----*/
		res = dwarf_attr(next_die,DW_AT_upper_bound,&Size,&error);

		if(res == DW_DLV_OK)
		{
			/* ----  Interpret data as signed data ----*/
			res = dwarf_formsdata(Size,&data,&error);

			if(res == DW_DLV_OK)
			{
				/* ---- The Array size is data + 1 since the upper bound is the max index possible ---- */
				data++;
			}
			else
			{
				/* ----  Interpret data as unsigned data ----*/
				res = dwarf_formudata(Size,&data_u,&error);

				if(res == DW_DLV_OK)
				{
					data = (Dwarf_Signed)data_u+1;
				}
				else
				{
					OutLog("ERROR :  Error in function DIE_GetArraySize : dwarf_formudata and dwarf_formsdata failed\n");
					data =-1;
				}
			}
		}
	}
	else
	{
		OutLog("ERROR :  Error in function DIE_GetArraySize : Was expected that the die has a child (dwarf_child failed)\n");
		data =-1;
	}

	return data;
}


/*******************************************************************************************
 * Function    : DIE_GetDIEBaseTypeName
 * Description : REturn the name of the basetype
 *******************************************************************************************/
int DIE_GetDIEBaseTypeName(Dwarf_Debug dbg, Dwarf_Die die, char *varname )
{
	int   res  = 0;
	char *name = NULL;
	Dwarf_Error error;
	Dwarf_Attribute Encoding;
	Dwarf_Unsigned type;

	/* ---- Get the encoding information of the DIE ----- */
	res = dwarf_attr(die,DW_AT_encoding,&Encoding,&error);

	if(res == DW_DLV_OK)
	{
		res = dwarf_formudata(Encoding,&type,&error);

		if(res == DW_DLV_OK)
		{
			res = dwarf_get_ATE_name(type,(const char **)&name);
			if(res == DW_DLV_OK)
			{
				strcpy(varname,name);
				dwarf_dealloc(dbg,name,DW_DLA_STRING);
			}
			else
			{
				OutLog("ERROR : dwarf_get_ATE_name : No dwarf_get_ATE_name \n");
			}
		}
		else
		{
			OutLog("ERROR : DIE_GetDIEBaseTypeName : No dwarf_formudata \n");
		}
	}
	else
	{
		OutLog("ERROR : DIE_GetDIEBaseTypeName : No DW_AT_encoding \n");
	}

	return res;
}
/************************************************************************************
 * Function    : DIE_GetStaticAddress
 * Description : This function returns the static address of an object
 ************************************************************************************/
int DIE_GetStaticAddress(Dwarf_Debug dbg, Dwarf_Die die , Dwarf_Addr *Addr)
{
	Dwarf_Attribute Address;
	Dwarf_Error     error;
	Dwarf_Signed    lcnt;
	Dwarf_Locdesc **lbuf;
	Dwarf_Half      form=0;
	Dwarf_Unsigned  exprlen=0;
	Dwarf_Ptr       expr_ptr=0;
	Dwarf_Locdesc *llbuf;
	Dwarf_Half      adrsize=0;
	int res = 0;

	/* ---- Get the address ----- */
	res = dwarf_attr(die,DW_AT_location,&Address,&error);

	if(res == DW_DLV_OK)
	{
		res = dwarf_whatform(Address,&form,&error);

		if(res == DW_DLV_OK)
		{
			switch(form)
			{
			case DW_FORM_exprloc:
			{
				res = dwarf_formexprloc(Address,&exprlen,&expr_ptr,&error);

				if(res == DW_DLV_OK)
				{
					res = dwarf_get_address_size(dbg,&adrsize,&error);

					if(res == DW_DLV_OK)
					{
						res = dwarf_loclist_from_expr_a(dbg,expr_ptr,exprlen,adrsize,&llbuf,&lcnt,&error);

						if(res == DW_DLV_OK)
						{
							if((lcnt>=1)&&(llbuf->ld_cents ==1)&&((llbuf->ld_s[0]).lr_number >0))
							{
								*Addr = (llbuf->ld_s[0]).lr_number;
							}
							else
							{

							}
							/* ---- De-allocate the descriptor ---*/
							dwarf_dealloc(dbg,llbuf->ld_s,DW_DLA_BLOCK);
							dwarf_dealloc(dbg,llbuf,DW_DLA_LOCDESC);
						}
						else
						{
							OutLog("ERROR : Error in function DIE_GetStaticAddress : dwarf_loclist_from_expr_a failed\n");
						}
					}
					else
					{
						OutLog("ERROR : Error in function DIE_GetStaticAddress : dwarf_get_address_size failed\n");
					}

				}
				else
				{
					OutLog("ERROR : Error in function DIE_GetStaticAddress : dwarf_formexprloc\n");
				}
			}break;
			case DW_FORM_block:
			case DW_FORM_block1:
			{
				res = dwarf_loclist_n(Address,&lbuf,&lcnt,&error);

				if(res == DW_DLV_OK)
				{
					if((lcnt>=1)&&(lbuf[0]->ld_cents ==1))
					{
						*Addr = (lbuf[0]->ld_s[0]).lr_number;
					}
					/* ---- De-allocate the descriptor ---*/
					dwarf_dealloc(dbg,lbuf[0],DW_DLA_LOCDESC);
				}
				else
				{
					OutLog("ERROR : Error in function DIE_GetStaticAddress : dwarf_loclist_n failed\n");
				}
			}break;
			default :
			{
				OutLog("ERROR : Error in function DIE_GetStaticAddress : Unexpected form (0x%x)\n ",form);
			}break;
			}
		}
	}
	else
	{

	}

	return res;
}


/************************************************************************************
 * Function    : DIE_GetFnStaticAddress
 * Description : This function returns the static address of an function
 ************************************************************************************/
void DIE_GetFnStaticAddress(Dwarf_Debug dbg, Dwarf_Die die , Dwarf_Addr *Addr_hi,Dwarf_Addr *Addr_low)
{
	Dwarf_Attribute Address;
	Dwarf_Error     error;
	int res = 0;

	/* ---- Get the address ----- */
	res = dwarf_attr(die,DW_AT_high_pc,&Address,&error);

	if(res == DW_DLV_OK)
	{
		res = dwarf_formaddr(Address,Addr_hi,&error);

		if(res == DW_DLV_OK)
		{

		}
		else
		{
			OutLog("ERROR : Error in function DIE_GetFnStaticAddress : dwarf_formaddr(DW_AT_high_pc) failed\n");
		}
	}
	else
	{
		OutLog("ERROR : Error in function DIE_GetFnStaticAddress  no DW_AT_high_pc found\n");
	}


	/* ---- Get the address ----- */
	res = dwarf_attr(die,DW_AT_low_pc,&Address,&error);

	if(res == DW_DLV_OK)
	{
		res = dwarf_formaddr(Address,Addr_low,&error);

		if(res == DW_DLV_OK)
		{

		}
		else
		{
			OutLog("ERROR : Error in function DIE_GetFnStaticAddress : dwarf_formaddr(DW_AT_low_pc) failed\n");
		}
	}
	else
	{
		OutLog("ERROR : Error in function DIE_GetFnStaticAddress  no DW_AT_low_pc found\n");
	}

}


/*******************************************************************************************
 * Function    : DIE_GetDIEBaseTypeName
 * Description : Return of the size in bytes of the DIE
 *******************************************************************************************/
int DIE_GetDIESize(Dwarf_Debug dbg, Dwarf_Die die, Dwarf_Unsigned *size)
{
	int   res  = 0;
	Dwarf_Error     error;
	Dwarf_Attribute Attr;
	Dwarf_Unsigned  size_loc=0;
	Dwarf_Signed    size_loc_s=0;

	/* ---- Get the attribute of the DIE ----- */
	res = dwarf_attr(die,DW_AT_byte_size,&Attr,&error);

	if(res == DW_DLV_OK)
	{
		res = dwarf_formudata(Attr,&size_loc,&error);

		if(res == DW_DLV_OK)
		{
			*size = size_loc;
		}
		else
		{
			OutLog("DEBUG : DIE_GetDIESize : dwarf_formudata not found\n");

			res = dwarf_formsdata(Attr,&size_loc_s,&error);

			if(res == DW_DLV_OK)
			{
				if(size_loc_s >= 0)
				{
					*size = (Dwarf_Unsigned)size_loc_s;
				}
				else
				{
					OutLog("ERROR : DIE_GetDIESize : dwarf_formsdata size = %d bytes\n",(int)size_loc_s);
				}
			}
			else
			{
				OutLog("DEBUG : DIE_GetDIESize : dwarf_formsdata not found\n");
			}
		}
	}
	else
	{
		OutLog("DEBUG : DIE_GetDIESize : DW_AT_byte_size not found\n");
	}
	return res;
}
/*****************************************************************************
 * Function    : DIEGetDieName
 * Description : copies the name of the die in the die_name string
 ***************************************************************************/
int DIE_GetDIE_Name(Dwarf_Debug dbg, Dwarf_Die die,char *die_name)
{
	char *name = NULL;
	Dwarf_Error     error;
	int   res  = 0;

	res = dwarf_diename(die,&name,&error);

	if(res == DW_DLV_OK)
	{
		/* ---- Copy the name in the output string ----*/
		strcpy(die_name,name);
		/* ---- De-alloc the string ----*/
		dwarf_dealloc(dbg,name,DW_DLA_STRING);

	}
	return res;
}
/*****************************************************************************
 * Function    : DIE_GetDIELocation
 * Description : returns the attribute location as an offset
 ***************************************************************************/
void DIE_GetDIELocation(Dwarf_Debug dbg, Dwarf_Die die,Dwarf_Signed *off)
{
	Dwarf_Unsigned  offset = 0u;
	Dwarf_Signed    offset_signed = 0;
	Dwarf_Attribute Attr;
	Dwarf_Error     error;
	Dwarf_Half      form=0;
	int             res =0;

	/* ---- Get the member location of the variable ----*/
	res = dwarf_attr(die,DW_AT_data_member_location,&Attr,&error);

	if(res == DW_DLV_OK)
	{
		res = dwarf_whatform(Attr,&form,&error);

		if(res == DW_DLV_OK)
		{
			switch(form)
			{
			case DW_FORM_sdata:
			{
				OutLog("INFO : DIE_GetDIELocation : DW_FORM_sdata\n");

				res = dwarf_formsdata(Attr,&offset_signed,&error);

				if(res == DW_DLV_OK)
				{
					*off = offset_signed;
				}
				else
				{
					OutLog("ERROR : DIE_GetDIELocation :DW_FORM_sdata: Unable to create a signed data (dwarf_formsdata) \n");
				}

			}break;
			case DW_FORM_block:
			case DW_FORM_block1:
			{
				Dwarf_Locdesc **locdescs=NULL;
				Dwarf_Signed len;
				OutLog("DEBUG : DIE_GetDIELocation : DW_FORM_block/DW_FORM_block1\n");

				if(dwarf_loclist_n(Attr,&locdescs,&len,&error) == DW_DLV_OK)
				{
					if((len == 1)||(locdescs[0]->ld_cents == 1)||((locdescs[0]->ld_s[0]).lr_atom == DW_OP_plus_uconst))
					{
						*off = (locdescs[0]->ld_s[0]).lr_number;
					}
				}
				else
				{
					OutLog("ERROR : DIE_GetDIELocation : Unsupported member offset!\n");
				}
			}break;
			case DW_FORM_data1:
			case DW_FORM_data2:
			case DW_FORM_data4:
			case DW_FORM_data8:
			case DW_FORM_udata:
			{
				res = dwarf_formudata(Attr,&offset,&error);

				if(res == DW_DLV_OK)
				{
					*off = offset;
				}
				else
				{
					OutLog("ERROR : DIE_GetDIELocation : DW_FORM_data: Unable to create a unsigned data (dwarf_formudata) \n");
				}

			}break;
			default :
			{
				*off = 0;
				OutLog("ERROR : DIE_GetDIELocation :  Unknown form (dwarf_whatform)\n");
			}break;
			}
		}
		else
		{
			OutLog("ERROR : DIE_GetDIELocation : Function dwarf_whatform failed !\n");
		}
	}
	else
	{
		OutLog("ERROR : DIE_GetDIELocation : No location information was found\n");
	}
}

/* ---- End of file ----*/
