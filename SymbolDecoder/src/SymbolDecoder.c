/*
 ============================================================================
 Name        : SymbolDecoder.c
 Author      : Vlad Vasiliu
 Version     :
 Copyright   : This Software  is under GPL3 License
 Description : Decode the Address/type and location information of a symbol.
 ============================================================================
 */
/*Copyright (C) 2019  Vlad Vasiliu

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h> /* For open() */
#include <sys/stat.h> /* For open() */
#include <fcntl.h> /* For open() */
#include <stdlib.h> /* For exit() */
#include <unistd.h> /* For close() */
#include <stdio.h>
#include <errno.h>
#include "dwarf.h"
#include "libdwarf.h"
#include "die_func.h"
#include "die_ifx.h"

/* ---- For linux systems  ignore the binary flag ---- */
#ifndef  O_BINARY
#define  O_BINARY 0
#endif

static void Read_CU_list(Dwarf_Debug dbg,const char *symbol);
static int  Inspect_CU_DIE(Dwarf_Debug dbg, Dwarf_Die cu_die,const char *symbol);

int
main(int argc, char **argv)
{
	/* ---- Opaque Data for the DWARF engine ----*/
	Dwarf_Debug dbg = 0;
	/* ---- File Descriptor in ----*/
	int fd    = -1;
	/* ---- File name ----*/
	const char *filepath = NULL;
	const char *symbol   = NULL;

	int res = DW_DLV_ERROR;

	Dwarf_Error   error;
	Dwarf_Handler errhand = 0;
	Dwarf_Ptr     errarg = 0;

	if(argc < 3)
	{
		printf("ERROR : Parameters Not found !\n");
		exit(1);

	}
	else
	{
		/* ---- Open Elf file ----*/
		filepath = argv[1];
		symbol   = argv[2];

		printf("INFO : Elf File : %s\n",filepath);
		printf("INFO : Symbol   : %s\n",symbol);

	}

	/* ---- Open file -----*/
	fd = open(filepath,O_RDONLY | O_BINARY);

	/* ----- Check for the correct opening ----- */
	if(fd < 0)
	{
		printf("ERROR : Failure in opening  %s\n",filepath);
		exit(1);
	}



	/* ---- Init the DWARF library --- */
	res = dwarf_init(fd,DW_DLC_READ,errhand,errarg, &dbg,&error);

	if(res != DW_DLV_OK)
	{
		printf("ERROR : Error in  DWARF processing\n");
		exit(1);
	}


	/* ---- Read all compile units list ---- */
	Read_CU_list(dbg,symbol);


	/* ---- Close the DWARF processing ---- */
	res = dwarf_finish(dbg,&error);

	if(res != DW_DLV_OK)
	{
		printf("ERROR : dwarf_finish failed!\n");
	}

	/* ---- Close the File Descriptors ---- */
	close(fd);
	return 0;
}

static void Read_CU_list(Dwarf_Debug dbg,const char *symbol)
{
	Dwarf_Unsigned cu_header_length = 0;
	Dwarf_Half     version_stamp    = 0;
	Dwarf_Unsigned abbrev_offset    = 0;
	Dwarf_Half     address_size     = 0;
	Dwarf_Unsigned next_cu_header = 0;
	Dwarf_Error    error;
	int cu_number = 0;

	/* ---- For all Compiler units (*.c files ) do ----- */
	for(;;++cu_number)
	{
		Dwarf_Die no_die = 0;
		Dwarf_Die cu_die = 0;
		int res = DW_DLV_ERROR;

		/* ----- Read CU header ---- */
		res = dwarf_next_cu_header(dbg,&cu_header_length,
				&version_stamp, &abbrev_offset, &address_size,
				&next_cu_header, &error);

		if(res == DW_DLV_ERROR)
		{
			printf("ERROR : Error in dwarf_next_cu_header: %s\n",dwarf_errmsg(error));
			exit(1);
		}

		if(res == DW_DLV_NO_ENTRY)
		{
			/* Done. */
			return;
		}

		/* The CU will have a single sibling, a cu_die. */
		res = dwarf_siblingof(dbg,no_die,&cu_die,&error);
		if(res == DW_DLV_ERROR)
		{
			printf("ERROR : Error in dwarf_siblingof on CU die :%s\n",dwarf_errmsg(error));
			exit(1);
		}

		if(res == DW_DLV_NO_ENTRY)
		{
			/* Impossible case. */
			printf("ERROR : No entry! in dwarf_siblingof on CU die :%s\n",dwarf_errmsg(error));
			exit(1);
		}

		if(Inspect_CU_DIE(dbg,cu_die,symbol)!=0)
		{
			/* ---- Symbol was found ----*/
			break;
		}

		dwarf_dealloc(dbg,cu_die,DW_DLA_DIE);
	}
}

;


static int Inspect_CU_DIE(Dwarf_Debug dbg, Dwarf_Die cu_die,const char *symbol)
{
	int       res      = DW_DLV_ERROR;
	int       found    = 0;
	Dwarf_Die old_die  = NULL;
	Dwarf_Die new_die  = NULL;
	Dwarf_Error error  = 0;

	CU_PrintName(dbg, cu_die);

	res = dwarf_child(cu_die,&new_die,&error);



	/* ---- For all the die's  do ----*/
	while((res== DW_DLV_OK) && (found==0))
	{


		found |= DIE_HandleFunction(dbg,new_die,symbol);
		found |= DIE_HandleVariable(dbg,new_die,symbol);
		found |= DIE_HandleType    (dbg,new_die,symbol);
		old_die = new_die;


		res = dwarf_siblingof(dbg,old_die,&new_die,&error);

		/* ---- De alloc DIE ---- */
		dwarf_dealloc(dbg,old_die,DW_DLA_DIE);
	}

	return found;
}
