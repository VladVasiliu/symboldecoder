/*Copyright (C) 2019  Vlad Vasiliu

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef DIE_FUNC_H_
#define DIE_FUNC_H_


int DIE_HandleFunction(Dwarf_Debug dbg, Dwarf_Die die,const char *symbol);
int DIE_HandleVariable(Dwarf_Debug dbg, Dwarf_Die die,const char *symbol);
int DIE_HandleType    (Dwarf_Debug dbg, Dwarf_Die die,const char *symbol);


#endif /* DIE_FUNC_H_ */
